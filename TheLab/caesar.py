alphabet = "abcdefghijklmnopqrstuvwxyz"
cryptogram = raw_input(": ").lower()
possibilities = list()
for key in range(len(alphabet)):
    dec = ""
    for n in cryptogram:
        if alphabet.find(n) >= 0:
            dec = dec + alphabet[alphabet.index(n)-key]
        else:
            dec = dec + n
    possibilities.append(dec)
for n in range(len(possibilities)): #prints out the possibilites with the shift used
    print n, possibilities[n]
